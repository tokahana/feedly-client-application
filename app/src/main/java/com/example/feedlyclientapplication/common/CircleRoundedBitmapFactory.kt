package com.example.feedlyclientapplication.common

import android.content.res.Resources
import android.graphics.Bitmap
import androidx.core.graphics.drawable.RoundedBitmapDrawable
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import kotlin.math.max

class CircleRoundedBitmapFactory {
    companion object {
        fun create(res: Resources, bitmap: Bitmap): RoundedBitmapDrawable {
            val drawable = RoundedBitmapDrawableFactory.create(res, bitmap)
            drawable.cornerRadius = max(bitmap.width, bitmap.height) / 2.0f
            return drawable
        }
    }
}