package com.example.feedlyclientapplication.common

import android.util.Log

class AppLog {
    companion object {

        private const val TAG = "FCA"

        fun d(msg: String) {
            Log.d(TAG, msg)
        }

        fun i(msg: String) {
            Log.i(TAG, msg)
        }
    }
}