package com.example.feedlyclientapplication.common

import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.view.MenuItem
import androidx.core.graphics.drawable.toBitmap
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition

class MenuItemTarget(
    private val menuItem: MenuItem,
    private val res: Resources
) : CustomTarget<Drawable>() {

    override fun onResourceReady(
        drawable: Drawable,
        transition: Transition<in Drawable>?
    ) {
        menuItem.icon = CircleRoundedBitmapFactory.create(res, drawable.toBitmap())
    }

    override fun onLoadCleared(placeholder: Drawable?) {}
}
