package com.example.feedlyclientapplication.viewModel

import androidx.lifecycle.*
import com.example.feedlyclientapplication.model.Category
import com.example.feedlyclientapplication.model.Entry
import com.example.feedlyclientapplication.model.Profile
import com.example.feedlyclientapplication.model.Stream
import com.example.feedlyclientapplication.repository.FeedlyRepository
import kotlinx.coroutines.launch

class EntryListViewModel(private val feedlyRepo: FeedlyRepository) : ViewModel() {

    private var currentStream: Stream? = null
    private val error: MutableLiveData<Exception> = MutableLiveData()
    private val profile: MutableLiveData<Profile> = MutableLiveData()
    private val entryList: MutableLiveData<List<Entry>> = MutableLiveData()
    private val currentEntry: MutableLiveData<Entry> = MutableLiveData()

    fun getError(): LiveData<Exception> {
        return error
    }

    fun getProfile(): LiveData<Profile> {
        return profile
    }

    fun loadProfile() {
        viewModelScope.launch {
            try {
                profile.value = feedlyRepo.getProfile()
            } catch (e: Exception) {
                error.value = e
            }
        }
    }

    fun getEntryList(): LiveData<List<Entry>> {
        return entryList
    }

    fun loadEntryList(category: Category) {
        viewModelScope.launch {
            try {
                val stream = feedlyRepo.getAllStreamContents(category)
                currentStream = stream
                entryList.value = stream.items
            } catch (e: Exception) {
                error.value = e
            }
        }
    }

    fun loadNextEntryList() {
        viewModelScope.launch {
            try {
                currentStream?.let {
                    val stream = feedlyRepo.getNextStreamContents(it)
                    currentStream = stream
                    entryList.value = entryList.value?.plus(stream.items)
                }
            } catch (e: Exception) {
                error.value = e
            }
        }
    }

    fun getCurrentEntry(): LiveData<Entry> {
        return currentEntry
    }

    fun setCurrentEntry(entry: Entry) {
        currentEntry.value = entry
    }
}

class EntryListViewModelFactory(private val feedlyRepo: FeedlyRepository) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == EntryListViewModel::class.java) {
            return EntryListViewModel(feedlyRepo) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class : ${modelClass.name}")
    }

}
