package com.example.feedlyclientapplication.viewModel

import androidx.lifecycle.*
import com.example.feedlyclientapplication.model.Profile
import com.example.feedlyclientapplication.repository.FeedlyRepository
import kotlinx.coroutines.launch

class ProfileViewModel(private val feedlyRepo: FeedlyRepository) : ViewModel() {

    private val error: MutableLiveData<Exception> = MutableLiveData()
    private val profile: MutableLiveData<Profile> = MutableLiveData()

    fun getError(): LiveData<Exception> {
        return error
    }

    fun getProfile(): LiveData<Profile> {
        return profile
    }

    fun loadProfile() {
        viewModelScope.launch {
            try {
                profile.value = feedlyRepo.getProfile()
            } catch (e: Exception) {
                error.value = e
            }
        }
    }
}

class ProfileViewModelFactory(private val feedlyRepo: FeedlyRepository) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == ProfileViewModel::class.java) {
            return ProfileViewModel(feedlyRepo) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class : ${modelClass.name}")
    }

}
