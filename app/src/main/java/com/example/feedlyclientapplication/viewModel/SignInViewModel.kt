package com.example.feedlyclientapplication.viewModel

import android.net.Uri
import androidx.lifecycle.*
import com.example.feedlyclientapplication.model.AuthToken
import com.example.feedlyclientapplication.repository.FeedlyRepository
import kotlinx.coroutines.launch


class SignInViewModel(private val feedlyRepo: FeedlyRepository) : ViewModel() {

    private val error: MutableLiveData<Exception> = MutableLiveData()
    private val isSignIn: LiveData<Boolean> = liveData { emit(feedlyRepo.isSignIn()) }
    private val authToken: MutableLiveData<AuthToken> = MutableLiveData()

    fun getError(): LiveData<Exception> {
        return error
    }

    fun getIsSignIn(): LiveData<Boolean> {
        return isSignIn
    }

    fun getSignInUri(): Uri {
        return feedlyRepo.getSignInUri()
    }

    fun getAuthToken(): LiveData<AuthToken> {
        return authToken
    }

    fun signOut() {
        feedlyRepo.removeAuthToken()
    }

    fun onPageUrlChanged(url: String?) {
        val uri = Uri.parse(url)
        if (uri.host != "localhost") {
            return
        }

        val code = uri.getQueryParameter("code")
        if (code !== null) {
            loadAuthToken(code)
        }
    }

    private fun loadAuthToken(code: String) {
        viewModelScope.launch {
            try {
                val token = feedlyRepo.getAuthToken(code)
                feedlyRepo.saveAuthToken(token)
                authToken.value = token
            } catch (e: Exception) {
                error.value = e
            }
        }
    }

}

class SignInViewModelFactory(private val feedlyRepo: FeedlyRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == SignInViewModel::class.java) {
            return SignInViewModel(feedlyRepo) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class : ${modelClass.name}")
    }

}
