package com.example.feedlyclientapplication.di

import android.content.Context
import com.example.feedlyclientapplication.repository.*
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideFeedlyRepository(context: Context): FeedlyRepository {
        val baseUriString = "https://cloud.feedly.com/"
        val authTokenClient =
            FeedlyAuthTokenClient(context)

        val logging = HttpLoggingInterceptor(FeedlyApiLogger())
        logging.level = HttpLoggingInterceptor.Level.BASIC

        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()

        val feedlyApiClient = Retrofit.Builder()
            .client(client)
            .baseUrl(baseUriString)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(FeedlyApiClient::class.java)

        return FeedlyRepositoryImpl(
            baseUriString = baseUriString,
            feedlyAuthTokenClient = authTokenClient,
            feedlyApiClient = feedlyApiClient
        )
    }

}