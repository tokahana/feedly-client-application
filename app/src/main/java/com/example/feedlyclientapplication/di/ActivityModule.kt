package com.example.feedlyclientapplication.di

import com.example.feedlyclientapplication.view.main.MainActivity
import com.example.feedlyclientapplication.view.signIn.SignInActivity
import com.example.feedlyclientapplication.view.start.StartActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    internal abstract fun contributeStartActivity(): StartActivity

    @ContributesAndroidInjector
    internal abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun contributeSignInActivity(): SignInActivity
}