package com.example.feedlyclientapplication.di

import com.example.feedlyclientapplication.repository.FeedlyRepository
import com.example.feedlyclientapplication.viewModel.EntryListViewModelFactory
import com.example.feedlyclientapplication.viewModel.ProfileViewModelFactory
import com.example.feedlyclientapplication.viewModel.SignInViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ViewModelModule {

    @Singleton
    @Provides
    fun provideEntryListViewModelFactory(feedlyRepository: FeedlyRepository): EntryListViewModelFactory {
        return EntryListViewModelFactory(feedlyRepository)
    }

    @Singleton
    @Provides
    fun provideProfileViewModelFactory(feedlyRepository: FeedlyRepository): ProfileViewModelFactory {
        return ProfileViewModelFactory(feedlyRepository)
    }


    @Singleton
    @Provides
    fun provideSignInViewModelFactory(feedlyRepository: FeedlyRepository): SignInViewModelFactory {
        return SignInViewModelFactory(feedlyRepository)
    }

}
