package com.example.feedlyclientapplication.di

import com.example.feedlyclientapplication.view.main.EntryFragment
import com.example.feedlyclientapplication.view.main.EntryListFragment
import com.example.feedlyclientapplication.view.signIn.SignInFragment
import com.example.feedlyclientapplication.view.signIn.SignInWebViewFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    internal abstract fun contributeEntryFragment(): EntryFragment

    @ContributesAndroidInjector
    internal abstract fun contributeEntryListFragment(): EntryListFragment

    @ContributesAndroidInjector
    internal abstract fun contributeSignInFragment(): SignInFragment

    @ContributesAndroidInjector
    internal abstract fun contributeSignInWebViewFragment(): SignInWebViewFragment
}