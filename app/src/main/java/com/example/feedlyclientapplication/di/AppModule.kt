package com.example.feedlyclientapplication.di

import android.content.Context
import com.example.feedlyclientapplication.view.AppApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideContext(application: AppApplication): Context {
        return application.applicationContext
    }
}