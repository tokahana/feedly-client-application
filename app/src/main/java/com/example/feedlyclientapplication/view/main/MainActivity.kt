package com.example.feedlyclientapplication.view.main

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.feedlyclientapplication.R
import com.example.feedlyclientapplication.common.MenuItemTarget
import com.example.feedlyclientapplication.model.Profile
import com.example.feedlyclientapplication.view.common.BaseActivity
import com.example.feedlyclientapplication.view.signIn.SignInActivity
import com.example.feedlyclientapplication.viewModel.ProfileViewModel
import com.example.feedlyclientapplication.viewModel.ProfileViewModelFactory
import com.example.feedlyclientapplication.viewModel.SignInViewModel
import com.example.feedlyclientapplication.viewModel.SignInViewModelFactory
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var signInViewModelFactory: SignInViewModelFactory

    @Inject
    lateinit var profileViewModelFactory: ProfileViewModelFactory

    private lateinit var signInViewModel: SignInViewModel
    private lateinit var profileViewModel: ProfileViewModel
    private lateinit var mainMenu: Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        signInViewModel = ViewModelProvider(this, signInViewModelFactory)
            .get(SignInViewModel::class.java)

        profileViewModel = ViewModelProvider(this, profileViewModelFactory)
            .get(ProfileViewModel::class.java)

        observeViewModel()
        loadViewModelData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu?.let {
            menuInflater.inflate(R.menu.main_menu, menu)
            mainMenu = menu
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.signOutMenu -> {
                signOut()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun observeViewModel() {
        profileViewModel.getError().observe(this, Observer { e ->
            handleException(e)
        })

        profileViewModel.getProfile().observe(this, Observer { profile ->
            loadProfileMenuIcon(profile)
        })
    }

    private fun loadViewModelData() {
        profileViewModel.loadProfile()
    }

    private fun loadProfileMenuIcon(profile: Profile) {
        mainMenu.findItem(R.id.profileMenu)?.let {
            Glide.with(this)
                .load(profile.picture)
                .into(MenuItemTarget(it, resources))
        }
    }

    private fun signOut() {
        signInViewModel.signOut()
        finish()
        startActivity(Intent(this, SignInActivity::class.java))
    }

}
