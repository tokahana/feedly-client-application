package com.example.feedlyclientapplication.view.signIn


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.feedlyclientapplication.R
import com.example.feedlyclientapplication.view.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_sign_in.*

class SignInFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signinButton.setOnClickListener {
            Navigation
                .findNavController(it)
                .navigate(R.id.action_signInFragment_to_signInWebViewFragment)
        }
    }
}
