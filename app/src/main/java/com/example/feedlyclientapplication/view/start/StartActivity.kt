package com.example.feedlyclientapplication.view.start

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.feedlyclientapplication.R
import com.example.feedlyclientapplication.view.common.BaseActivity
import com.example.feedlyclientapplication.view.main.MainActivity
import com.example.feedlyclientapplication.viewModel.SignInViewModel
import com.example.feedlyclientapplication.viewModel.SignInViewModelFactory
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.schedule

class StartActivity : BaseActivity() {

    @Inject
    lateinit var signInViewModelFactory: SignInViewModelFactory

    private lateinit var signInViewModel: SignInViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        signInViewModel = ViewModelProvider(this, signInViewModelFactory)
            .get(SignInViewModel::class.java)

        initView()

        Timer().schedule(1500) {
            runOnUiThread { observeViewModel() }
        }
    }

    private fun initView() {
        supportActionBar?.hide()
    }

    private fun observeViewModel() {
        signInViewModel.getIsSignIn().observe(this, Observer { isSignIn ->
            if (isSignIn) {
                startMainActivity()
            } else {
                startSignInActivity()
            }
        })
    }

    private fun startMainActivity() {
        finish()
        startActivity(Intent(this, MainActivity::class.java))
    }
}
