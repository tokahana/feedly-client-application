package com.example.feedlyclientapplication.view.main


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.feedlyclientapplication.R
import com.example.feedlyclientapplication.model.CategoryGlobalAll
import com.example.feedlyclientapplication.view.common.BaseFragment
import com.example.feedlyclientapplication.viewModel.EntryListViewModel
import com.example.feedlyclientapplication.viewModel.EntryListViewModelFactory
import kotlinx.android.synthetic.main.fragment_entry_list.*
import javax.inject.Inject

class EntryListFragment : BaseFragment() {

    @Inject
    lateinit var entryListViewModelFactory: EntryListViewModelFactory

    private lateinit var entryListAdapter: EntryListRecyclerViewAdapter
    private lateinit var entryListViewModel: EntryListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        entryListViewModel = ViewModelProvider(requireActivity(), entryListViewModelFactory)
            .get(EntryListViewModel::class.java)

        entryListAdapter = EntryListRecyclerViewAdapter(
            onClickItem = { entry -> entryListViewModel.setCurrentEntry(entry) },
            onClickFooter = { entryListViewModel.loadNextEntryList() }
        )

        observeViewModel()
        loadViewModelData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_entry_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initView()
    }

    private fun initView() {
        entryListRecyclerView.let {
            it.setHasFixedSize(true)
            it.layoutManager = LinearLayoutManager(activity)
            it.adapter = entryListAdapter
        }
    }

    private fun observeViewModel() {
        entryListViewModel.getError().observe(this, Observer { e ->
            handleError(e)
        })

        entryListViewModel.getProfile().observe(this, Observer { profile ->
            val category = CategoryGlobalAll(profile)
            entryListViewModel.loadEntryList(category)
        })

        entryListViewModel.getEntryList().observe(this, Observer { entryList ->
            entryListAdapter.submitList(entryList)
        })

        entryListViewModel.getCurrentEntry().observe(this, Observer { entry ->
            val action =
                EntryListFragmentDirections.actionEntryListFragmentToEntryFragment(entry)
            findNavController().navigate(action)
        })
    }

    private fun loadViewModelData() {
        entryListViewModel.loadProfile()
    }
}