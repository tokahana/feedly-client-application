package com.example.feedlyclientapplication.view.signIn

import android.os.Bundle
import com.example.feedlyclientapplication.R
import com.example.feedlyclientapplication.view.common.BaseActivity

class SignInActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
    }
}
