package com.example.feedlyclientapplication.view.signIn


import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.feedlyclientapplication.R
import com.example.feedlyclientapplication.view.common.BaseFragment
import com.example.feedlyclientapplication.view.main.MainActivity
import com.example.feedlyclientapplication.viewModel.SignInViewModel
import com.example.feedlyclientapplication.viewModel.SignInViewModelFactory
import kotlinx.android.synthetic.main.fragment_sign_in_web_view.*
import javax.inject.Inject

class SignInWebViewFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: SignInViewModelFactory

    private lateinit var viewModel: SignInViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(SignInViewModel::class.java)

        observeViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_in_web_view, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initView()
    }

    private fun initView() {
        initWebView(webView)
    }

    private fun observeViewModel() {
        viewModel.getError().observe(this, Observer {
            handleError(it)
        })

        viewModel.getAuthToken().observe(this, Observer {
            activity?.apply {
                finish()
                startActivity(Intent(this, MainActivity::class.java))
            }
        })
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView(webView: WebView) {
        webView.apply {
            webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(
                    view: WebView?,
                    request: WebResourceRequest?
                ): Boolean {
                    return false
                }

                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    viewModel.onPageUrlChanged(url)
                }
            }
            // NOTE:
            // Google does not accept oauth requests from webview clients.
            // So we have to send dummy user agent to sign in.
            settings.userAgentString = settings.userAgentString.replace("; wv", "")
            settings.javaScriptEnabled = true
            loadUrl(viewModel.getSignInUri().toString())
        }
    }

}
