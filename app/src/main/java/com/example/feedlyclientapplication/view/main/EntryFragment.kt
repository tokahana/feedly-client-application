package com.example.feedlyclientapplication.view.main


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.navigation.fragment.navArgs
import com.example.feedlyclientapplication.R
import com.example.feedlyclientapplication.common.AppLog
import com.example.feedlyclientapplication.view.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_entry.*

class EntryFragment : BaseFragment() {

    private val args: EntryFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_entry, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initView()
    }

    override fun onDestroyView() {
        entryWebView.destroy()
        super.onDestroyView()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initView() {
        val articleUrl = args.entry.getArticleUrl()
        AppLog.d("articleUrl = $articleUrl")

        entryWebView.let {
            it.webViewClient = EntryWebViewClient(
                _onPageFinished = { progressBar?.visibility = View.GONE }
            )
            it.settings.javaScriptEnabled = true
            it.loadUrl(articleUrl)
        }
    }

    private class EntryWebViewClient(
        private val _onPageFinished: () -> Unit
    ) : WebViewClient() {
        override fun shouldOverrideUrlLoading(
            view: WebView?,
            request: WebResourceRequest?
        ): Boolean {
            return false
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)

            _onPageFinished()
        }
    }
}
