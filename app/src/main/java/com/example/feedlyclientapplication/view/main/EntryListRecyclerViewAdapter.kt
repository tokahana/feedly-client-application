package com.example.feedlyclientapplication.view.main

import com.bumptech.glide.Glide
import com.example.feedlyclientapplication.R
import com.example.feedlyclientapplication.model.Entry
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.adapter_entry_list_item.view.*

class EntryListRecyclerViewAdapter(
    private val onClickItem: (entry: Entry) -> Unit,
    private val onClickFooter: () -> Unit
) : GroupAdapter<GroupieViewHolder>() {

    private val section = Section()

    init {
        section.setFooter(EntryItemListFooter(onClickFooter))
        add(section)
    }

    fun submitList(entryList: List<Entry>) {
        val entryItems = entryList.map { entry ->
            EntryItem(entry, onClickItem)
        }
        section.update(entryItems)
    }
}

class EntryItemListFooter(
    private val onClick: () -> Unit
) : Item() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.setOnClickListener {
            onClick()
        }
    }

    override fun getLayout(): Int {
        return R.layout.adapter_entry_list_footer_item
    }
}

class EntryItem(
    private val entry: Entry,
    private val onClick: (entry: Entry) -> Unit
) : Item() {

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.let {
            it.setOnClickListener { onClick(entry) }

            it.streamItemTitle.text = entry.title
            it.streamItemOriginTitle.text = entry.origin.title

            Glide
                .with(it.context)
                .load(entry.getFirstThumbnail()?.url)
                .placeholder(R.drawable.ic_image_black_24dp)
                .into(it.entryThumbnailImage.apply { clipToOutline = true })
        }
    }

    override fun getLayout(): Int {
        return R.layout.adapter_entry_list_item
    }
}
