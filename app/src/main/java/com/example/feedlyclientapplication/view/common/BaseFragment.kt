package com.example.feedlyclientapplication.view.common

import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment() {

    protected fun handleError(e: Exception) {
        val activity = requireActivity() as? BaseActivity
        activity?.handleException(e)
    }
}