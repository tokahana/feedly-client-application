package com.example.feedlyclientapplication.view.common

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import com.example.feedlyclientapplication.common.AppLog
import com.example.feedlyclientapplication.view.signIn.SignInActivity
import dagger.android.support.DaggerAppCompatActivity
import retrofit2.HttpException

abstract class BaseActivity : DaggerAppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)

        AppLog.d("onCreate ${this::class.java.simpleName}")
    }

    fun handleException(e: Exception) {
        when (e) {
            is HttpException -> startSignInActivity()
            else -> {
                Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    protected fun startSignInActivity() {
        finish()
        startActivity(Intent(this, SignInActivity::class.java))
    }
}
