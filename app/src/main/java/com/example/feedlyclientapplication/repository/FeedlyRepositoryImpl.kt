package com.example.feedlyclientapplication.repository

import android.net.Uri
import com.example.feedlyclientapplication.model.*

interface FeedlyRepository {
    fun isSignIn(): Boolean
    fun getSignInUri(): Uri
    fun saveAuthToken(authToken: AuthToken)
    fun removeAuthToken()
    suspend fun getAuthToken(code: String): AuthToken
    suspend fun getProfile(): Profile
    suspend fun getAllStreamContents(category: Category): Stream
    suspend fun getNextStreamContents(stream: Stream): Stream
}

class FeedlyRepositoryImpl(
    private val baseUriString: String,
    private val feedlyAuthTokenClient: FeedlyAuthTokenClient,
    private val feedlyApiClient: FeedlyApiClient
) : FeedlyRepository {

    override fun isSignIn(): Boolean {
        return feedlyAuthTokenClient.hasToken()
    }

    override fun getSignInUri(): Uri {
        return Uri.parse(baseUriString).buildUpon().run {
            path("/v3/auth/auth")
            appendQueryParameter("response_type", "code")
            appendQueryParameter("client_id", "feedly")
            appendQueryParameter("redirect_uri", "http://localhost")
            appendQueryParameter("scope", "https://cloud.feedly.com/subscriptions")
            appendQueryParameter("state", "state.passed.in")
            build()
        }
    }

    override fun saveAuthToken(authToken: AuthToken) {
        feedlyAuthTokenClient.saveAuthToken(authToken)
    }

    override fun removeAuthToken() {
        feedlyAuthTokenClient.removeAuthToken()
    }

    override suspend fun getAuthToken(code: String): AuthToken {
        val authTokenInput = AuthTokenInput(
            code = code,
            client_id = "feedly",
            client_secret = "0XP4XQ07VVMDWBKUHTJM4WUQ",
            redirect_uri = "http://localhost",
            state = "state.passed.in",
            grant_type = "authorization_code"
        )
        return feedlyApiClient.getAuthToken(authTokenInput)
    }

    override suspend fun getProfile(): Profile {
        return feedlyApiClient.getProfile(
            authHeader = getAuthorizationHeaderValue()
        )
    }

    override suspend fun getAllStreamContents(category: Category): Stream {
        return feedlyApiClient.getStreamContents(
            streamId = category.id,
            authHeader = getAuthorizationHeaderValue()
        )
    }

    override suspend fun getNextStreamContents(stream: Stream): Stream {
        return feedlyApiClient.getStreamContents(
            streamId = stream.id,
            continuation = stream.continuation,
            authHeader = getAuthorizationHeaderValue()
        )
    }

    private fun getAuthorizationHeaderValue(): String {
        val authToken = feedlyAuthTokenClient.getAuthToken()
        return "Bearer ${authToken.access_token}"
    }

}