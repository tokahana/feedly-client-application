package com.example.feedlyclientapplication.repository

import android.content.Context
import android.preference.PreferenceManager
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import androidx.core.content.edit
import com.example.feedlyclientapplication.model.AuthToken
import com.google.gson.Gson
import java.math.BigInteger
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec
import javax.security.auth.x500.X500Principal

open class FeedlyAuthTokenClient(private val context: Context) {

    private val keyStoreAlias = "Feedly_Access_Token"
    private val authTokenKey = "FEEDLY_ACCESS_TOKEN"
    private val authTokenIvKey = "FEEDLY_ACCESS_TOKEN_IV"

    open fun getAuthToken(): AuthToken {
        return Gson().fromJson(getText(authTokenKey, authTokenIvKey), AuthToken::class.java)
    }

    open fun saveAuthToken(authToken: AuthToken) {
        saveText(authToken.toJson(), authTokenKey, authTokenIvKey)
    }

    open fun removeAuthToken() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        prefs.edit {
            remove(authTokenKey)
            remove(authTokenIvKey)
        }
    }

    open fun hasToken(): Boolean {
        return try {
            getAuthToken()
            true
        } catch (e: AccessTokenNotFoundException) {
            false
        }
    }

    private fun saveText(tokenText: String, tokenKey: String, tokenIvKey: String) {
        val keyStore = KeyStore.getInstance("AndroidKeyStore").apply {
            load(null)
        }

        if (!keyStore.containsAlias(keyStoreAlias)) {
            generateKey()
        }

        val secretKey = keyStore.getKey(keyStoreAlias, null) as SecretKey

        val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)

        val encryptedText = Base64.encodeToString(
            cipher.doFinal(tokenText.toByteArray(Charsets.UTF_8)),
            Base64.NO_WRAP
        )
        val iv = Base64.encodeToString(cipher.iv, Base64.NO_WRAP)

        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        prefs.edit {
            putString(tokenKey, encryptedText)
            putString(tokenIvKey, iv)
        }
    }

    private fun getText(tokenKey: String, tokenIvKey: String): String {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val encryptToken = prefs.getString(tokenKey, null)
        val iv = prefs.getString(tokenIvKey, null)

        if (encryptToken == null || iv == null) {
            throw AccessTokenNotFoundException("Access Token has not been found.")
        }

        val keyStore = KeyStore.getInstance("AndroidKeyStore").apply {
            load(null)
        }

        val secretKey = keyStore.getKey(keyStoreAlias, null) as SecretKey

        val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
        val ivParameterSpec =
            IvParameterSpec(Base64.decode(iv, Base64.NO_WRAP))
        cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec)

        return String(
            cipher.doFinal(Base64.decode(encryptToken, Base64.NO_WRAP)),
            Charsets.UTF_8
        )
    }

    private fun generateKey() {
        val keyGenerator = KeyGenerator.getInstance(
            KeyProperties.KEY_ALGORITHM_AES,
            "AndroidKeyStore"
        )

        val parameterSpec: KeyGenParameterSpec = KeyGenParameterSpec.Builder(
            keyStoreAlias,
            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
        ).run {
            setCertificateSubject(X500Principal("CN=$keyStoreAlias"))
            setCertificateSerialNumber(BigInteger.ONE)
            setBlockModes(KeyProperties.BLOCK_MODE_CBC)
            setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
            setRandomizedEncryptionRequired(false)
            build()
        }

        keyGenerator.init(parameterSpec)
        keyGenerator.generateKey()
    }
}