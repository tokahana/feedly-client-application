package com.example.feedlyclientapplication.repository

import com.example.feedlyclientapplication.model.AuthToken
import com.example.feedlyclientapplication.model.AuthTokenInput
import com.example.feedlyclientapplication.model.Profile
import com.example.feedlyclientapplication.model.Stream
import retrofit2.http.*

class AccessTokenNotFoundException(message: String) : Exception(message)

interface FeedlyApiClient {
    @POST("v3/auth/token")
    suspend fun getAuthToken(
        @Body authTokenInput: AuthTokenInput
    ): AuthToken

    @GET("v3/profile")
    suspend fun getProfile(
        @Header("Authorization") authHeader: String
    ): Profile

    @GET("v3/streams/contents")
    suspend fun getStreamContents(
        @Query("streamId") streamId: String,
        @Query("continuation") continuation: String? = null,
        @Header("Authorization") authHeader: String
    ): Stream
}
