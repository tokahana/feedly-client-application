package com.example.feedlyclientapplication.repository

import com.example.feedlyclientapplication.common.AppLog
import okhttp3.logging.HttpLoggingInterceptor

class FeedlyApiLogger : HttpLoggingInterceptor.Logger {
    override fun log(message: String) {
        AppLog.d(message)
    }
}