package com.example.feedlyclientapplication.model

import java.io.Serializable
import java.math.BigInteger

data class Entry(
    val id: String,
    val keywords: List<String>,
    val originId: String,
    val fingerprint: String,
    val thumbnail: List<Thumbnail>?,
    val title: String,
    val author: String,
    // val summary:
    val alternate: List<Alternate>?,
    val crawled: BigInteger,
    val published: BigInteger,
    val origin: Origin,
    val visual: Visual,
    val unread: Boolean
    // val categories
) : Serializable {

    fun getArticleUrl(): String? {
        return alternate?.get(0)?.href
    }

    fun getFirstThumbnail(): Thumbnail? {
        return thumbnail?.get(0)
    }
}
