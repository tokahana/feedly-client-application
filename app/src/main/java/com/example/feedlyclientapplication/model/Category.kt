package com.example.feedlyclientapplication.model

open class Category(
    val id: String,
    val label: String
)

class CategoryGlobalAll(
    profile: Profile
) : Category(
    id = "user/${profile.id}/category/global.all",
    label = "global.all"
)