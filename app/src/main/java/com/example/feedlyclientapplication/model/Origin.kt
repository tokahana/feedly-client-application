package com.example.feedlyclientapplication.model

import java.io.Serializable

data class Origin(
    val streamId: String,
    val title: String,
    val htmlUrl: String
) : Serializable