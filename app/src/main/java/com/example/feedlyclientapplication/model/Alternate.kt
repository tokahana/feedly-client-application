package com.example.feedlyclientapplication.model

import java.io.Serializable

data class Alternate(
    val href: String,
    val type: String
) : Serializable