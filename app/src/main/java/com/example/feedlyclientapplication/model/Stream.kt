package com.example.feedlyclientapplication.model

import java.math.BigInteger

data class Stream(
    val id: String,
    val updated: BigInteger,
    val continuation: String,
    val items: List<Entry>
)
