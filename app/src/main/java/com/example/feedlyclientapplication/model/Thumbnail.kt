package com.example.feedlyclientapplication.model

import java.io.Serializable

data class Thumbnail(
    val url: String
) : Serializable