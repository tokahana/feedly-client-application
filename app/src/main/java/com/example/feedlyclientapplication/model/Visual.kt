package com.example.feedlyclientapplication.model

import java.io.Serializable
import java.math.BigInteger

data class Visual(
    val url: String,
    val width: Int? = null,
    val height: Int?  = null,
    val contentType: String?  = null,
    val processor: String?  = null,
    val edgeCacheUrl: String?  = null,
    val expirationDate: BigInteger? = null
) : Serializable