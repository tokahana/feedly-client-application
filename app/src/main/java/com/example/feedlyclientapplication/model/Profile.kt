package com.example.feedlyclientapplication.model

import java.io.Serializable

data class Profile(
    val id: String,
    val email: String,
    val picture: String
) : Serializable