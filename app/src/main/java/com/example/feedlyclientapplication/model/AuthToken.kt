package com.example.feedlyclientapplication.model

import com.google.gson.Gson

data class AuthToken(
    val id: String,
    val refresh_token: String,
    val access_token: String,
    val expires_in: Int,
    val token_type: String,
    val plan: String,
    val state: String
) {
    fun toJson(): String {
        return Gson().toJson(this)
    }
}

