package com.example.feedlyclientapplication.model

data class AuthTokenInput(
    val code: String,
    val client_id: String,
    val client_secret: String,
    val redirect_uri: String,
    val state: String,
    val grant_type: String
)