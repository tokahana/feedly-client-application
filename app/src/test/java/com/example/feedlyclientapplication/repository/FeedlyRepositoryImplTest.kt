package com.example.feedlyclientapplication.repository

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test

class FeedlyRepositoryImplTest {

    private lateinit var baseUriString: String
    private lateinit var feedlyAuthTokenClientMock: FeedlyAuthTokenClient
    private lateinit var feedlyApiClientMock: FeedlyApiClient
    private lateinit var feedlyRepositoryImpl: FeedlyRepositoryImpl

    @Before
    fun setUp() {
        baseUriString = "http://example.com"
        feedlyAuthTokenClientMock = mock()
        feedlyApiClientMock = mock()
        feedlyRepositoryImpl = FeedlyRepositoryImpl(
            baseUriString = baseUriString,
            feedlyAuthTokenClient = feedlyAuthTokenClientMock,
            feedlyApiClient = feedlyApiClientMock
        )
    }

    @After
    fun tearDown() { }

    @Test
    fun isSignIn_givenAuthTokenClientHasToken_thenReturnTrue() {
        whenever(feedlyAuthTokenClientMock.hasToken()).thenReturn(true)

        assertThat(feedlyRepositoryImpl.isSignIn()).isTrue()
    }

    @Test
    fun isSignIn_givenAuthTokenClientDoesNotHaveToken_thenReturnFalse() {
        whenever(feedlyAuthTokenClientMock.hasToken()).thenReturn(false)

        assertThat(feedlyRepositoryImpl.isSignIn()).isFalse()
    }
}