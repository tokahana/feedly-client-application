package com.example.feedlyclientapplication.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.feedlyclientapplication.model.AuthToken
import com.example.feedlyclientapplication.repository.FeedlyRepository
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [28])
class SignInViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var signInViewModel: SignInViewModel
    private lateinit var feedlyRepoMock: FeedlyRepository

    @Before
    fun setUp() {
        feedlyRepoMock = mock()
        signInViewModel = SignInViewModel(feedlyRepoMock)
    }

    @After
    fun tearDown() { }

    @Test
    fun onPageUrlChanged_givenLocalhostUrl_thenDoNothing() = runBlocking {
        val code = "XXXXXYYYYYZZZZZ"

        val observer = mock<Observer<AuthToken>>()
        signInViewModel.getAuthToken().observeForever(observer)
        signInViewModel.onPageUrlChanged("http://example.com?code=$code")

        verify(observer, never()).onChanged(any())
        verify(feedlyRepoMock, never()).saveAuthToken(any())
    }

    @Test
    fun onPageUrlChanged_givenNoCodeParameter_thenDoNothing() = runBlocking {
        val observer = mock<Observer<AuthToken>>()
        signInViewModel.getAuthToken().observeForever(observer)
        signInViewModel.onPageUrlChanged("http://localhost")

        verify(observer, never()).onChanged(any())
        verify(feedlyRepoMock, never()).saveAuthToken(any())
    }

    @Test
    fun onPageUrlChanged_givenUrlHasCodeParameter_thenAuthTokenWillBeSetAndSaved() = runBlocking {
        val code = "XXXXXYYYYYZZZZZ"
        val authToken = createDummyAuthToken()

        doReturn(authToken).whenever(feedlyRepoMock).getAuthToken(code)
        doNothing().whenever(feedlyRepoMock).saveAuthToken(any())

        val observer = mock<Observer<AuthToken>>()
        signInViewModel.getAuthToken().observeForever(observer)
        signInViewModel.onPageUrlChanged("http://localhost?code=$code")

        verify(observer).onChanged(authToken)
        verify(feedlyRepoMock).saveAuthToken(authToken)
    }

    private fun createDummyAuthToken(): AuthToken {
        return AuthToken(
            id = "ABC",
            refresh_token = "ABC",
            access_token = "ABC",
            expires_in = 123,
            token_type = "ABC",
            plan = "ABC",
            state = "ABC"
        )
    }

}